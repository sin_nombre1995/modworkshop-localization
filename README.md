# modworkshop-localization

A public repository for localizing modworkshop.net


## My language has a special quirk not found in English
You'll have to contact me (Luffy) via Discord so we can solve it. 

I've already solved an issue with slavic languages' counters; You'll find 'slavic.yaml' in Russian and Polish that contain strings for time that change depending on the number.

However, when possible, please try to solve the issue without special cases for me to code.

## How should I translate?

Generally, the translations should be semi-formal (especially English).

There are some languages that get an exception. 
But if the speakers of that language feel it needs to be more formal/casual, then feel free to push a merge request.

We are a modding site after all, not some business fancy shmancy site.